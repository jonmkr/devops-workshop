import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals(201, calculatorResource.calculate(expression));

        expression = "40            /10";
        assertEquals(4, calculatorResource.calculate(expression));

        expression = "1*353";
        assertEquals(353, calculatorResource.calculate(expression));

        expression = "124 + 1 + 53";
        assertEquals(178, calculatorResource.calculate(expression));

        expression = "155 - 124 - 60";
        assertEquals(-29, calculatorResource.calculate(expression));

        expression = "45 * 12 * 1";
        assertEquals(540, calculatorResource.calculate(expression));

        expression = "16 / 4 / 2";
        assertEquals(2, calculatorResource.calculate(expression));

        expression = "1 + 2 + 3 + 4 + 5";
        assertEquals(15, calculatorResource.calculate(expression));

    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));

        expression = "200+339+1";
        assertEquals(540, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "35*62";
        assertEquals(2170, calculatorResource.multiplication(expression));

        expression = "27*12345679";
        assertEquals(333333333, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "164/4";
        assertEquals(41, calculatorResource.division(expression));

        expression = "2/3";
        assertEquals(0, calculatorResource.division(expression));

        expression = "1337/0";
        assertEquals(-1, calculatorResource.division(expression));
    }
}
