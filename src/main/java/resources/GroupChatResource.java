package resources;

import data.GroupChat;
import data.Message;
import java.util.ArrayList;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import dao.GroupChatDAO;

/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {
    GroupChatDAO gchat = new GroupChatDAO();

    /**
     * GET method to get one groupchat with specified groupChatId
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path ("{groupChatId}")
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId) {
        GroupChat groupChat = new GroupChat();

        groupChat.setGroupChatId(groupChatId);
        groupChat.setGroupChatName(gchat.getGroupChat(groupChatId).getGroupChatName());
        groupChat.setMessageList(gchat.getGroupChatMessages(groupChatId));
        groupChat.setUserList(gchat.getGroupChatUsers(groupChatId));

        return groupChat;
    }



    @GET
    @Path("user/{userId}")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getGroupChatByUser(@PathParam("userId") int userId) {
        return gchat.getGroupChatByUserId(userId);
    }

    @POST
    @Consumes (MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat postGroupChat(GroupChat groupChat) {
        return gchat.addGroupChat(groupChat);
    }

    @GET
    @Path("{groupChatId}/message")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<Message> getGroupChatMessagesByGroupID(@PathParam("groupChatId") int groupChatId) {
        return gchat.getGroupChatMessages(groupChatId);
    }

    @POST
    @Path("{groupChatId}/message")
    @Consumes (MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public Message postMessage(@PathParam("groupChatId") int groupChatId, Message message) {
        return gchat.addMessage(groupChatId, message);
    }
}
